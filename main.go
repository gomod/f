package f

import (
	"fmt"

	"gitlab.com/gomod/g"
)

func Say(l int) {
	fmt.Printf("%v%v\n", "                       "[:l*2], "F: 1.1.0")
	g.Say(l + 1)
}
